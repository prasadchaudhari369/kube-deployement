from flask import Flask, request, jsonify
from flask_cors import CORS
from google.cloud import firestore

app = Flask(__name__)
CORS(app)

# Initialize Firestore
db = firestore.Client()

@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/login', methods=['POST'])
def login():
    try:
        data = request.get_json()
        name = data.get('name')
        password = data.get('password')
        doc_ref = db.collection('CompanyInfo').add({
            'name': name,
            'password': password,
        })
        return 'Data saved successfully', 200
    except Exception as e:
        print('Error saving data:', e)
        return 'Error saving data', 500

@app.route('/getUserInfo', methods=['GET'])
def get_user_info():
    try:
        company_info_collection = db.collection('CompanyInfo').get()
        company_info_list = []

        for doc in company_info_collection:
            company_data = doc.to_dict()
            company_info_list.append({
                'id': doc.id,
                'name': company_data['name'],
                'password': company_data['password'],
                # Add other fields as needed
            })

        return jsonify(company_info_list), 200
    except Exception as e:
        print('Error retrieving data:', e)
        return 'Error retrieving data', 500

if __name__ == '__main__':
    app.run(port=5001)

