const express = require('express');
const bodyParser = require('body-parser');
const admin = require("firebase-admin");
const cors = require("cors");
const app = express();
app.use(express.json());
app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }));
app.use(cors());
const serviceAccount =  require('./serviceAccountKey.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://generated-wharf-389611-default-rtdb.firebaseio.com"
  });
  const db = admin.firestore();
const port = 5001;

app.get('/', (req, res)=>{
    res.send("Hello World!");
})

app.post('/login', async(req, res) => {
    try {
        const {name, password} = req.body;
        const data = {name, password};
        await admin.firestore().collection("CompanyInfo").add(data);
        res.status(200).send('Data saved successfully');
    } catch (error) {
        console.error('Error saving data:', error);
        res.status(500).send('Error saving data');
    }
})

app.get('/getUserInfo', async (req, res) => {
    try {
        const companyInfoCollection = await admin.firestore().collection("CompanyInfo").get();
        const companyInfoList = [];
        
        companyInfoCollection.forEach((doc) => {
            const companyData = doc.data();
            companyInfoList.push({
                id: doc.id,
                name: companyData.name,
                password: companyData.password,
                // Add other fields as needed
            });
        });

        res.status(200).json(companyInfoList);
    } catch (error) {
        console.error('Error retrieving data:', error);
        res.status(500).send('Error retrieving data');
    }
});



app.listen(port, () =>{
   console.log( `server started at http://localhost:${port}`);
})